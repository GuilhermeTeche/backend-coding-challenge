# API REST INVESTMENT FUNDS

## Resumo de tecnologias utilizadas e Deploy.
- *Java 8*
- *Spring Boot*
- *Junit (testes unitários e integrados)*
- *Mockito*
- *LOGS na aplicação (SLF4J)*
- *Swagger*

Para execução e testes do projeto podemos seguir de duas formas:
- *Postman* ou
- *Swagger*

Antes de mais nada, deve executar a aplicação com o seguinte comando:
```
mvn spring-boot:run
```
Este comando deve ser executado na raiz de onde o projeto foi baixado. Após executar este comando a aplicação será inicializada na porta 9898.

## SWAGGER ![alt text](https://leadsbridge.com/wp-content/themes/leadsbridge/img/integration-lg-logos/logo840.png)
- Para ficar mais fácil testes e não ficar dependendo de uma ferramenta instalada na máquina loca, a aplicação já contempla o Swagger. Para utilizá-lo, após ter iniciado o projeto com o comando citado acima basta acessar a seguinte URL em seu navegador:
```
http://localhost:9898/swagger-ui.html
```

Após ter acessado o link, basta seguir os seguintes passos:
- Clique em founds-investments-controller;
- Clique em GET /founds;
- Clique no botão "Try it out";
- E por fim, clique em Execute.

Após este procedimento, será apresentado uma lista com os 10 fundos com maior captação liquida no mês de Julho. 

## Postman ![alt text](https://lh3.googleusercontent.com/8T1sK3krF94U1CQhVHMHUwtdRrrJtawv00SHulg0CAqYVKshjpchTfPpTuct745aysJbS94V=w128-h128-e365)
- Caso desejar testar pelo Postman, basta criar uma nova configuração apontando para a seguinte url:
```
http://localhost:9898/founds
```
Utilizando o tipo de requisição GET.

## Logs ![alt text](https://img.icons8.com/dotty/2x/log.png)
- A aplicação contém logs info's e logs errors via console para conseguir visualizar os steps.


A Aplicação está com uma cobertura de testes unitários de no mínimo 90%.

Obrigado!

  


