package com.ciandt.investment.dataprovider;

import com.ciandt.investment.core.controller.FoundsInvestimentsController;
import com.ciandt.investment.core.domain.InformeDiario;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class InformeDiarioGatewayTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FoundsInvestimentsController foundsInvestimentsController;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(foundsInvestimentsController).build();
    }

    @Test
    public void sanity() throws IOException {
        InformeDiarioGateway informeDiarioGateway = new InformeDiarioGateway();
        List<InformeDiario> informeDiarios = informeDiarioGateway.getAll();
        assertEquals(325850, informeDiarios.size());
    }

    @Test
    public void testGetPublicKeyIsHttpStatusCode200() throws Exception {
        this.mockMvc.perform(get("/founds")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}