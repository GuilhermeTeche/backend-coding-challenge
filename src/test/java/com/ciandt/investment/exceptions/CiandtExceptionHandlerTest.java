package com.ciandt.investment.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CiandtExceptionHandlerTest {

    public static final String ENTRY_AND_EXIT_CORROMPTY = "Entrada-Saída/Serialização-Desserialização interrompida!";
    public static final String CLASS_NOT_FOUND_IN_CLASSPATH = "Falha ao tentar carregar classe, não encontrou a classe solicitada no classpath!";
    public static final String PARSER_ERROR = "Erro atingido inesperadamente durante a análise do parser!";

    @InjectMocks
    private CiandtExceptionHandler ciandtExceptionHandler;

    @Mock
    private WebRequest webRequest;

    @Mock
    private MessageSource messageSource;

    @Mock
    private IOException ioException;

    @Mock
    private ClassNotFoundException classNotFoundException;

    @Mock
    private ParseException parseException;

    @Mock
    private JsonProcessingException jsonprocessingexception;

    @Before
    public void setUp() {
        Map<String, String[]> mockParameterMap = new HashMap<>();
        mockParameterMap.put("teste", new String[]{"teste01"});
        when(webRequest.getParameterMap()).thenReturn(mockParameterMap);
    }


    @Test
    public void testHandleIOException() {
        when(ioException.getMessage()).thenReturn(ENTRY_AND_EXIT_CORROMPTY);
        ResponseEntity<Object> responseEntity = ciandtExceptionHandler.handleIOException(ioException, webRequest);
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void testHandleClassNotFoundException() {
        when(classNotFoundException.getMessage()).thenReturn(CLASS_NOT_FOUND_IN_CLASSPATH);
        ResponseEntity<Object> responseEntity = ciandtExceptionHandler.handleClassNotFoundException(classNotFoundException, webRequest);
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void testHandleParseException() {
        when(parseException.getMessage()).thenReturn(PARSER_ERROR);
        ResponseEntity<Object> responseEntity = ciandtExceptionHandler.handleParseException(parseException, webRequest);
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void testHandleJsonProcessingException() {
        ResponseEntity<Object> responseEntity = ciandtExceptionHandler.handleJsonProcessingException(jsonprocessingexception, webRequest);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }
}