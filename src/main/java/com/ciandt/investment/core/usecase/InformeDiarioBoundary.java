package com.ciandt.investment.core.usecase;

import com.ciandt.investment.core.domain.InformeDiario;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface InformeDiarioBoundary {
    List<InformeDiario> getAll() throws IOException;
    String getTeenFoundsWithTheHighestNetFundingInJuly() throws IOException, ParseException;
}
