package com.ciandt.investment.core.service.impl;

import com.ciandt.investment.core.service.FoundsInvestmentsService;
import com.ciandt.investment.dataprovider.InformeDiarioGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;

@Service
public class FoundsInvestimentsServiceImpl implements FoundsInvestmentsService {

    @Autowired
    private InformeDiarioGateway informeDiarioGateway;

    public String findFoundsInvestiments() throws IOException, ParseException {
        return informeDiarioGateway.getTeenFoundsWithTheHighestNetFundingInJuly();
    }
}
