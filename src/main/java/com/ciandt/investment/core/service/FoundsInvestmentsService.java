package com.ciandt.investment.core.service;

import java.io.IOException;
import java.text.ParseException;

public interface FoundsInvestmentsService {
    String findFoundsInvestiments() throws IOException, ParseException;
}
