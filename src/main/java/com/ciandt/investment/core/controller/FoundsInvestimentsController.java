package com.ciandt.investment.core.controller;

import com.ciandt.investment.core.service.impl.FoundsInvestimentsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping(value = "/founds", produces = MediaType.APPLICATION_JSON_VALUE)
public class FoundsInvestimentsController {

    @Autowired
    private FoundsInvestimentsServiceImpl foundsInvestimentsServiceImpl;

    @GetMapping
    public ResponseEntity<String> findTopTeenFoundsInvestments() throws IOException, ParseException {
        return ResponseEntity.ok(foundsInvestimentsServiceImpl.findFoundsInvestiments());
    }
}
