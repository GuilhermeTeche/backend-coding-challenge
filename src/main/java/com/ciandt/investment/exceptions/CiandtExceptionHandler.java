package com.ciandt.investment.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

@Slf4j
@ControllerAdvice
public class CiandtExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler({IOException.class})
    public ResponseEntity<Object> handleIOException(IOException ex, WebRequest request) {
        String messageUser = messageSource.getMessage("message.ioexception", null, LocaleContextHolder.getLocale());
        String messageDeveloper = ex.toString();
        List<Error> errors = Arrays.asList(new Error(messageUser, messageDeveloper));
        log.error(messageDeveloper);
        return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({ClassNotFoundException.class})
    public ResponseEntity<Object> handleClassNotFoundException(ClassNotFoundException ex, WebRequest request) {
        String messageUser = messageSource.getMessage("message.classnotfound", null, LocaleContextHolder.getLocale());
        String messageDeveloper = ex.toString();
        List<Error> errors = Arrays.asList(new Error(messageUser, messageDeveloper));
        log.error(messageDeveloper);
        return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({ParseException.class})
    public ResponseEntity<Object> handleParseException(ParseException ex, WebRequest request) {
        String messageUser = messageSource.getMessage("message.parseexception", null, LocaleContextHolder.getLocale());
        String messageDeveloper = ex.toString();
        List<Error> errors = Arrays.asList(new Error(messageUser, messageDeveloper));
        log.error(messageDeveloper);
        return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({JsonProcessingException.class})
    public ResponseEntity<Object> handleJsonProcessingException(JsonProcessingException ex, WebRequest request) {
        String messageUser = messageSource.getMessage("message.jsonprocessingexception", null, LocaleContextHolder.getLocale());
        String messageDeveloper = ex.toString();
        List<Error> errors = Arrays.asList(new Error(messageUser, messageDeveloper));
        log.error(messageDeveloper);
        return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    public static class Error {
        private String messageUser;
        private String messageDeveloper;

        public Error(String messageUser, String messageDeveloper) {
            this.messageUser = messageUser;
            this.messageDeveloper = messageDeveloper;
        }
    }
}

