package com.ciandt.investment.dataprovider;

import com.ciandt.investment.core.domain.InformeDiario;
import com.ciandt.investment.core.usecase.InformeDiarioBoundary;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class InformeDiarioGateway implements InformeDiarioBoundary {

    private static final int HEADER_LINE = 1;
    private static final String SEPARATOR = ";";
    private static final String MONTH_07 = "07";
    private static final String SRC_MAIN_RESOURCES_INFORMES = "src/main/resources/informes";
    private static final String FILE_CSV = "inf_diario_fi_201907.csv";
    private static final String FORMAT_DATE_CSV = "yyyy-mm-dd";
    private static final String FORMAT_EXPECTED = "mm";

    /**
     * @return List<InformeDiario>
     */
    @Override
    public List<InformeDiario> getAll() throws IOException {
        BufferedReader dataset = new BufferedReader(getReader(getPath()));
        return dataset
                .lines()
                .skip(HEADER_LINE)
                .map(InformeDiario::new)
                .collect(Collectors.toList());
    }

    /**
     * @return String
     */
    @Override
    public String getTeenFoundsWithTheHighestNetFundingInJuly() throws IOException, ParseException {
        log.info("starting search for investment funds...");

        String line;
        BufferedReader br = new BufferedReader(getReader(getPath()));

        br.readLine();
        ArrayList<BigDecimal> captationLiquid = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            String[] found = line.split(SEPARATOR);
            if (getMonthJuly(found[1]).equals(MONTH_07)) {
                captationLiquid.add(getCaptationLiquid(found));
            }
        }
        return new Gson().toJson(ordenateDescAndLasTeenFunds(captationLiquid));
    }

    /**
     * @param captationLiquid
     * @return ArrayList<BigDecimal>
     */
    private List<BigDecimal> ordenateDescAndLasTeenFunds(ArrayList<BigDecimal> captationLiquid) {
        captationLiquid.sort(Collections.reverseOrder());
        log.info("returning 10 funds with higher funding ...");
        return captationLiquid.subList(0, 10);
    }

    /**
     * @param path
     * @return BufferedReader
     * @throws IOException
     */
    private BufferedReader getReader(Path path) throws IOException {
        return Files.newBufferedReader(path, StandardCharsets.UTF_8);
    }

    /**
     * @return Path
     */
    private Path getPath() {
        return Paths.get(SRC_MAIN_RESOURCES_INFORMES, FILE_CSV);
    }

    /**
     * @param s
     * @return String
     * @throws ParseException
     */
    private String getMonthJuly(String s) throws ParseException {
        return new SimpleDateFormat(FORMAT_EXPECTED).format(new SimpleDateFormat(FORMAT_DATE_CSV).parse(s));
    }

    /**
     * @param found
     * @return BigDecimal
     */
    private BigDecimal getCaptationLiquid(String[] found) {
        return new BigDecimal(found[5]).subtract(new BigDecimal(found[6]));
    }
}
